﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SnakeGame
{

    public class AudioManager : MonoBehaviour
    {

        public AudioSource pointSound;

        public void PlayPointCollectSound()
        {
            pointSound.Play();
        }

    }
}
