﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeGame
{
    public class DestroyFood : MonoBehaviour
    {



        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Food")
            {
                Destroy(other.gameObject);
            }
        }
    }
}