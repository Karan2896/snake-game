﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeGame
{
    public class FoodBehavior : MonoBehaviour
    {

        [Header("Snake Manager")]
        SnakeMovement SM;

        [Header("Food Amount")]
        public int foodAmount;

        // Use this for initialization
        void Start()
        {
            SM = GameObject.FindGameObjectWithTag("SnakeManager").GetComponent<SnakeMovement>();

            foodAmount = Random.Range(1, 10);

            transform.GetComponentInChildren<TextMesh>().text = "" + foodAmount;
        }

        // Update is called once per frame
        void Update()
        {
            if (SM.transform.childCount > 0 && transform.position.y - SM.transform.GetChild(0).position.y < -10)
                Destroy(this.gameObject);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Debug.Log("Something Overlapping " + this.gameObject);
            //Destroy(this.gameObject);
            if (collision.gameObject.tag == "SnakeHead" || collision.gameObject.tag == "Box" || collision.gameObject.tag == "SimpleBox")
                Destroy(this.gameObject);
        }


    }
}
