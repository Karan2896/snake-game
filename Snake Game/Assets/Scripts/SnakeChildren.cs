﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SnakeGame
{
    public class SnakeChildren : MonoBehaviour
    {

        public Transform leader;
        public float followSharpness = 0.1f;

        public float distanceWanted = 1.0f;

        SnakeMovement SM;
        public bool isleader;
        public int isChildIndex;

        void Start()
        {

            SM = GameObject.FindGameObjectWithTag("SnakeManager").GetComponent<SnakeMovement>();
            //Debug.Log(SM.BodyParts.Count);
            if (!isleader)
            {
                leader = SM.BodyParts[isChildIndex];
                // Cache the initial offset at time of load/spawn:
                //_followOffset = transform.position - leader.position;
                Vector3 diff = transform.position - leader.position;
                transform.position = leader.position + diff.normalized * distanceWanted;
            }
        }

        void LateUpdate()
        {
            if (!isleader && leader != null)
            {
                Vector3 diff = transform.position - leader.position;
                transform.position = leader.position + diff.normalized * distanceWanted;
            }

            // Smooth follow.    
            //transform.position += (targetPosition - transform.position) * followSharpness;
        }




    }
}
